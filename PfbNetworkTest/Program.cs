﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PfbNetworkTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var networkManager = new NetworkManager();

            Console.WriteLine("Press Esc to close application...");
            Console.WriteLine();
            do 
            {
                while (!Console.KeyAvailable)
                {
                    var internetConnectionStatus = networkManager.GetStatusOfNetworkUsedForInternet();
                    PrintNetworkStatus(internetConnectionStatus);
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                }       
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
        public static void DisplayTypeAndAddress()
        {
            var process = new Process
            {
                StartInfo =
                      {
                          FileName = "netsh.exe",
                          Arguments = "wlan show interfaces ",
                          UseShellExecute = false,
                          RedirectStandardOutput = true,
                          CreateNoWindow = true
                      }
            };
            process.Start();

            var output = process.StandardOutput.ReadToEnd();
            string result = process.StandardOutput.ReadToEnd();
            Console.WriteLine(output);
                                   
            var lanProcess = new Process
            {
                StartInfo =
                      {
                          FileName = "netsh.exe",                     
                          Arguments = "interface show interface name=\"Ethernet\" ",
                          UseShellExecute = false,
                          RedirectStandardOutput = true,
                          CreateNoWindow = true
                      }
            };
            lanProcess.Start();
           
            var lanOutput = lanProcess.StandardOutput.ReadToEnd();
            var lanState = lanOutput.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault(l => l.Contains("Connect state"));
            var wlanState = output.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault(l => l.Contains("State"));
            if (!wlanState.Contains("disconnected") && lanState.Contains("Disconnected"))

            {
                Console.WriteLine("Wi-Fi");
            }
            else if (wlanState.Contains("disconnected") && !lanState.Contains("Disconnected"))
            {
                Console.WriteLine("Ethernet");
            }
            else if (!wlanState.Contains("disconnected") && !lanState.Contains("Disconnected"))
            {
                Console.WriteLine("Wi-Fi & Ethernet");
            }
            else
            {
                Console.WriteLine("Not connected");
            }

            Console.WriteLine("=================================================");
  
            Console.Read();
            Console.WriteLine();
        }
        private static void PrintNetworkStatus(NetworkConnectionStatus connectionStatus)
        {
            if (connectionStatus == null)
            {
                Console.WriteLine("No internet connection");
            }
            else
            {
                Console.WriteLine($"Network: {connectionStatus.NetworkName}");
                Console.WriteLine($"Connected to Internet: {connectionStatus.IsInternetConnectionActive}");
                Console.WriteLine($"Network Type: {connectionStatus.ConnectionType}");
                Console.WriteLine($"Network Strength: {connectionStatus.SignalStrength}%");
                Console.WriteLine();
            }
        }
    }
}
