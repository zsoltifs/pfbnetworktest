﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using PFBNetwork;
namespace NetworkTestUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();

            Thread thread = new Thread(new ThreadStart(InvokedMethod));
            thread.Start();
        }


        public void Display()
        {
            try
            {
                Dispatcher.InvokeAsync((Action)(() =>
            {
                var networkManager = new NetworkManager();
                var internetConnectionStatus = networkManager.GetStatusOfNetworkUsedForInternet();
                SignalStrength(internetConnectionStatus);
            }));
            }
            catch
            { }

        }
        internal void InvokedMethod()
        {
            //int seconds = 5 * 1000;
            //var timer = new Timer(Display, null, 0, seconds);
            while (true)
            {
                Display();
                Thread.Sleep(1000 * 5); // 5 Second/Minutes *60
                                        //Have a break condition
            }

        }

        internal void SignalStrength(NetworkConnectionStatus networkConnectionStatus)
        {
            lblConnection.Content = "Name:=> Signal Strength= " + networkConnectionStatus.SignalStrength;
            string ImageTypeOrUrlWithOrWihotuExtension = ".png";
            string startupPath = AppDomain.CurrentDomain.BaseDirectory;
            string FolderPath = Path.GetFullPath(Path.Combine(startupPath, @"..\..\") + "\\Pictures\\");
            string SignalStrength = string.Empty;
            string ImageName = "PercentSignalStrenth_" + networkConnectionStatus.ConnectionType.ToString() + ImageTypeOrUrlWithOrWihotuExtension;

            switch (networkConnectionStatus.SignalStrength)
            {
                case int n when (n > 0 && n <= 25):
                    SignalStrength = "25";
                    break;
                case int n when (n >= 25 && n <= 50):
                    SignalStrength = "50";
                    break;
                case int n when (n >= 50 && n <= 75):
                    SignalStrength = "75";
                    break;
                case int n when (n >= 75 && n <= 100):
                    SignalStrength = "100";
                    break;
                default:
                    try
                    {
                        ImageName = "Disconnect_";
                    }
                    catch
                    {                    
                        ImageName = "Disconnect_Unknown";
                    }
                    break;
            }
            ImageTypeOrUrlWithOrWihotuExtension = FolderPath + SignalStrength + ImageName;
            ConnectivityImage.Source = new BitmapImage(new Uri(ImageTypeOrUrlWithOrWihotuExtension));
        }
    }


}
