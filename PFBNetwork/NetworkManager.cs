﻿using Microsoft.WindowsAPICodePack.Net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;

namespace PFBNetwork
{
    /// <summary>
    /// Manages and queries network connections on the device.
    /// </summary>
    public class NetworkManager
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool Wow64DisableWow64FsRedirection(ref IntPtr ptr);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool Wow64RevertWow64FsRedirection(IntPtr ptr);
        List<NetworkConnectionStatus> networkConnectionStatus = new List<NetworkConnectionStatus>();
        /// <summary>
        /// Queries the list of networks on the device.
        /// </summary>
        /// <returns>the status (name, connection type, etc.) of all available computer networks on the device</returns>
        public NetworkConnectionStatus[] GetAllAvailableNetworkStatus()
        {
            
            if (!NetworkInterface.GetIsNetworkAvailable())
                return null;
            try { 
            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                networkConnectionStatus.Add(new NetworkConnectionStatus
                {
                    Id = ni.Id,
                    ConnectionType = ParseEnum<NetworkConnectionType>(ni.NetworkInterfaceType.ToString()),
                    NetworkName = ni.Name,
                    IsInternetConnectionActive = ni.SupportsMulticast,
                    SignalStrength = (int)ni.Speed / 4 / 8 / 64 / 1024,
                    Description = ni.Description
                });
            }
            }
            catch(Exception ex)
            {
                Console.Write(ex.Message);
            }
            return networkConnectionStatus.ToArray();
        }
        /// <summary>
        /// Queries the status of the network which has active internet connection.
        /// </summary>
        /// <returns>the status of the network which the device uses to connect to the internet, null if the device is not connected to the internet</returns>
        public NetworkConnectionStatus GetStatusOfNetworkUsedForInternet()
        {

            var singalll = NetworkInterface.GetAllNetworkInterfaces();
            string NetworkType = string.Empty;
            NetworkConnectionStatus _networkConnectionStatus = new NetworkConnectionStatus();
            try
            {
                if (!NetworkInterface.GetIsNetworkAvailable())
                return null;
            else
            {

                foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
                {

                    // discard because of standard reasons
                    //if (!string.IsNullOrWhiteSpace(ConnectedNetworkId(ni.Id)))
                    if (ni.OperationalStatus.ToString() == "Up")
                    {
                        
                            switch(ni.Name)
                            {
                                case "Wi-Fi":
                                    NetworkType = ni.Name.ToString().Replace("Wi-Fi", "Wifi");
                                    break;
                                case string _type when _type.Contains("Ethernet"):
                                    NetworkType = "Ethernet";
                                    break;                            
                                default:
                                    NetworkType = ni.Name.ToString();
                                    break;
                            }
                           _networkConnectionStatus.Id = ni.Id;
                           _networkConnectionStatus.ConnectionType = ParseEnum<NetworkConnectionType>(NetworkType);                
                           _networkConnectionStatus.NetworkName = ConnectedNetworkId(ni.Id);                         
                           _networkConnectionStatus.IsInternetConnectionActive = ni.SupportsMulticast;                         
                           _networkConnectionStatus.SignalStrength = int.Parse(SignalStrenth("Signal",NetworkType));                        
                           _networkConnectionStatus.Description = ni.Description;

                        Console.WriteLine("=====*****=====");
                        return _networkConnectionStatus;
                    }
                }
            }

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return _networkConnectionStatus;
        }
        /// <summary>
        /// Casting or Parse NetworkConnectionType to Enum
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ParseEnum<T>(string value)
        {
            T result = default(T);
            try
            {
                result = (T)Enum.Parse(typeof(T), value, true);
            }
            catch
            {
                result = (T)Enum.Parse(typeof(T), "Unknown", true);
            }

            return result;

        }
        /// <summary>
        /// Fetching Connected Internet Connection or Network Name
        /// </summary>
        /// <param name="netWorkID"></param>
        /// <returns></returns>
        public string ConnectedNetworkId(string netWorkID)
        {
            var networks = NetworkListManager.GetNetworks(NetworkConnectivityLevels.Connected);
            string NetworkName = string.Empty;
            try { 
            foreach (Network network in networks)
            {
                foreach (NetworkConnection conn in network.Connections)
                {
                    //Print network interface's GUID
                    if (conn.IsConnected)
                        NetworkName = network.Name;
                }
            }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return NetworkName;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string SignalStrenth(string fieldValue,string ConnectionType=null)
        {
            try
            {

                IntPtr ptr = IntPtr.Zero;
                string netshArgs = "";
                Wow64DisableWow64FsRedirection(ref ptr);
                if (ConnectionType == "Cellular")
                    netshArgs = "mbn show signal interface=\"cellular\"";
                else
                    netshArgs = "wlan show interfaces";
                ProcessStartInfo procInfo = new ProcessStartInfo
                {
                    WorkingDirectory = Path.GetPathRoot(Environment.SystemDirectory),
                    FileName = Path.Combine(Environment.SystemDirectory, "netsh.exe"),
                    Arguments = netshArgs,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Hidden
                };

                Process proc = Process.Start(procInfo);
                proc.WaitForExit();
                #region Committed Code
                //Console.WriteLine(proc.StandardOutput.ReadToEnd());
                //Console.WriteLine("Welcome to the club");
                //Console.ReadLine();

                //Wow64RevertWow64FsRedirection(ptr);
                //Process p = new Process();
                //p.StartInfo.FileName = "netsh.exe";
                //if(ConnectionType== "Cellular")
                //{

                //    p.StartInfo.Arguments = "mbn show signal interface=cellular";
                //}
                //else
                //{
                //    p.StartInfo.Arguments = "wlan show interfaces";
                //}

                //p.StartInfo.UseShellExecute = false;
                //p.StartInfo.RedirectStandardOutput = true;
                //p.StartInfo.CreateNoWindow = true;
                //p.Start();
                //Console.WriteLine("Signal Strenth  0004 ==01 ");
                //string result = p.StandardOutput.ReadToEnd();
                //Console.WriteLine("Signal Strenth  0004 ==02 ");
                //p.WaitForExit();
                //Console.WriteLine("Signal Strenth  0004 == "+ result);
                #endregion

                string result = proc.StandardOutput.ReadToEnd();
                string signalStrenth = result.Substring(result.IndexOf("%") - 2, 3);

                //Console.WriteLine(signalStrenth);
                return signalStrenth.Replace("%","");
                //return result.Substring(result.IndexOf(fieldValue)).Substring(result.Substring(result.IndexOf(fieldValue))
                //    .IndexOf(":")).Substring(2, result.Substring(result.IndexOf(fieldValue)).Substring(result.Substring(result.IndexOf(fieldValue))
                //    .IndexOf(":")).IndexOf("\n")).Trim();
            }
            catch
            {
                return "";
            }
        }
    }

    /// <summary>
    /// Stores information about a computer network (name, type, signal strength, etc.) 
    /// </summary>
    public class NetworkConnectionStatus 
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// The name or SSID of the network. 
        /// </summary>
        public string NetworkName { get; set; }

        /// <summary>
        /// The connection type (Wifi, LAN, etc.)
        /// </summary>
        public NetworkConnectionType ConnectionType { get; set; }

        /// <summary>
        /// True if the device is connected to the internet through this connection, false otherwise.
        /// </summary>
        public bool IsInternetConnectionActive { get; set; }

        /// <summary>
        /// A number between 0 (poor) and 100 (strong) representing the strength of the network as percentage. Not used for Ethernet connections (always 0).
        /// </summary>
        public int SignalStrength { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }
    }

    /// <summary>
    /// Represents the medium on which the network is communicating through.
    /// </summary>
    public enum NetworkConnectionType 
    {
        /// <summary>
        /// Type of network medium is not known
        /// </summary>
        Unknown,

        /// <summary>
        /// LAN connection
        /// </summary>
        Ethernet,

        /// <summary>
        /// WLAN connection
        /// </summary>
        Wifi,
        /// <summary>
        /// 
        /// </summary>
        Cellular,
        /// <summary>
        /// 3G, 4G or other type of mobile network connection
        /// </summary>
        Mobile
    }
}
